FROM php:7.3-apache-buster
label MAINTAINER="Krerk Piromsopa, PH.D. <Krerk.P@chula.ac.th>"

COPY htdocs/ /var/www/html/
RUN mkdir /var/www/data
RUN chmod 777 /var/www/data
RUN echo "empty" > /var/www/data/.placeholder

COPY cluster-entrypoint /usr/local/bin
ENTRYPOINT ["cluster-entrypoint"]
CMD ["apache2-foreground"]
