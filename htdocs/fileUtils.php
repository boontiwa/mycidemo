<?php

/**
 * fileUtils.php
 * @author Krerk Piromsopa, Ph.D.
 */
function listFiles($DATA_DIR) {
    $list = [];
    if ($handle = opendir($DATA_DIR)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                $list[] = $entry;
            }
        }
        closedir($handle);
    }
    return $list;
}
